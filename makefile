CXX=c++
RM=rm -f
PREFIX=/usr/local
CXXFLAGS=--std=c++14 -pedantic -Wall -Wextra
LDFLAGS = -I include
LDFLAGS += -I tests
LDFLAGS += -I $(PREFIX)/include
LDLIBS=-L$(PREFIX)/lib -lscrypt

BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1

.POSIX: all debug opt

all: scry

debug: CXXFLAGS += -save-temps -g -O0
debug: clean scry

opt: CXXFLAGS += --pipe -O2 -fpie -march=native -fstack-protector-strong
opt: LDFLAGS += -pie -Wl,-z,relro -Wl,-z,now
opt: clean scry

scry: scry.o main.o
	$(CXX) $(LDFLAGS) -o scry scry.o main.o $(LDLIBS)

scry.o: scry.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(CPPFLAGS) -c scry.cpp

main.o: main.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(CPPFLAGS) -c main.cpp

clean:
	$(RM) *.o *.s *.ii

distclean: clean
	$(RM) *~ *.tar.gz scry test_scry

test: test_scry.o scry.o
	$(CXX) $(LDFLAGS) -o test_scry scry.o test_scry.o $(LDLIBS)

test_scry.o: tests/test_scry.cpp
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $(CXXFLAGS) -c tests/test_scry.cpp

install:
	cp ./scry $(BINDIR)
	cp ./scry.1 $(MANDIR)

uninstall:
	rm $(BINDIR)/scry
	rm $(MANDIR)/scry.1

# make a source archive for distribution
dist: distclean
	tar -czvf ../scry-source.tar.gz ./
	mv ../scry-source.tar.gz ./
