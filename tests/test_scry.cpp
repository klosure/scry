#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "scry.hpp"

TEST_CASE( "Empty password given", "[empty-pass]" ) {
  auto sp = std::make_unique<Scry>();
  REQUIRE( sp->make_key("", 4096, 8, 2) > 0 );
}

TEST_CASE( "Short password given", "[short-pass]" ) {
  auto sp = std::make_unique<Scry>();
  REQUIRE( sp->make_key("a", 4096, 8, 2) > 0 );
}

TEST_CASE( "Medium password given", "[medium-pass]" ) {
  auto sp = std::make_unique<Scry>();
  REQUIRE( sp->make_key("aaaaaaaaaa12341234", 4096, 8, 2) > 0 );
}

TEST_CASE( "Long password given", "[long-pass]" ) {
  auto sp = std::make_unique<Scry>();
  REQUIRE( sp->make_key("aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341!@#$%^&*()-=+\\|`~[]{};<>?", 4096, 8, 2) > 0 );
}

TEST_CASE( "Very long password given", "[short-pass]" ) {
  auto sp = std::make_unique<Scry>();
  REQUIRE( sp->make_key("aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341!@#$%^&*()-=\\+|`~[]{};<>?aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341234aaaaaaaaaa12341!@#$%^&*()-=+|`~[]{};<>?", 4096, 8, 2) > 0 );
}
