
/*  Scry implementation file
 *  author: klosure
 *  license: ISC
 */

#include "scry.hpp"

/* --- Constructors --- */


/* --- Member Functions --- */


int Scry::make_key(const char* password, uint64_t n, uint32_t r, uint32_t p) const {
  char dest[129];
  try {
    libscrypt_hash(&dest[0], password, n, r, p);
  }
  catch(...) {
    std::cout << "Caught exception during key creation!\n";
    return 0;
  }
  std::cout << "salt + key = " << dest << "\n";
  return 1;
}


int Scry::make_key(std::string salt, std::string password, uint64_t n, uint32_t r, uint32_t p) const {
  uint8_t dest[64];
  const char* ccsalt = salt.c_str();
  const char* ccpass = password.c_str();
  size_t saltlen = strlen(salt.c_str());
  size_t passlen = strlen(password.c_str());
  const uint8_t* upass = reinterpret_cast<const uint8_t*>(ccpass);
  const uint8_t* usalt = reinterpret_cast<const uint8_t*>(ccsalt);
  char mcf[128];
  try {
    libscrypt_scrypt(upass, passlen, usalt, saltlen, n, r, p, dest, sizeof(dest));
    std::string encoded_dest = cppcodec::base64_rfc4648::encode(dest, sizeof(dest));
    const char* cc_encoded_dest = encoded_dest.c_str();
    libscrypt_mcf(n, r, p, ccsalt, cc_encoded_dest, &mcf[0]);
  }
  catch(...) {
    std::cout << "Caught exception during key creation!\n";
    return 0;
  }
  std::cout << "salt + key = " << mcf << "\n";
  return 1;
}


// This version is called when we were given n, r, or p but not a salt..
int Scry::make_key(uint8_t* salt, size_t saltlen, std::string password, uint64_t n, uint32_t r, uint32_t p) const {
  uint8_t dest[64];
  const char* ccpass = password.c_str();
  size_t passlen = strlen(password.c_str());
  const uint8_t* upass = reinterpret_cast<const uint8_t*>(ccpass);
  char mcf[128];
  try {
    std::string encoded_salt = cppcodec::base64_rfc4648::encode(salt, saltlen);
    const char* cc_encoded_salt = encoded_salt.c_str();
    size_t cce_saltlen = sizeof(cc_encoded_salt)/sizeof(cc_encoded_salt[0]);
    const uint8_t* usalt = reinterpret_cast<const uint8_t*>(cc_encoded_salt);
    libscrypt_scrypt(upass, passlen, usalt, cce_saltlen, n, r, p, dest, sizeof(dest));
    std::string encoded_dest = cppcodec::base64_rfc4648::encode(dest, sizeof(dest));
    const char* cc_encoded_dest = encoded_dest.c_str();
    libscrypt_mcf(n, r, p, cc_encoded_salt, cc_encoded_dest, &mcf[0]);
  }
  catch(...) {
    std::cout << "Caught exception during key creation!\n";
    return 0;
  }
  std::cout << "salt + key = " << mcf << "\n";
  return 1;
}
