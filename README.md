![](logo.png)

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

This tool provides a quick way do generate a [Scrypt](https://en.wikipedia.org/wiki/Scrypt) key from the shell. The normal scrypt cli tool provided [here](https://www.tarsnap.com/scrypt.html) will encrypt and decrypt files for you interactively. But, if you simply want it to give you a key, it cannot. This tool outputs the resulting key in [MCF format](https://passlib.readthedocs.io/en/stable/modular_crypt_format.html)

### Dependencies
+ [libscrypt-dev](https://github.com/technion/libscrypt)
+ [C++14](https://en.cppreference.com/w/cpp/compiler_support) capable compiler (*e.g.* GCC 4.9 / Clang 3.4 or greater )
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)

### Installation
Simply run:

```bash
git clone https://gitlab.com/klosure/scry.git
cd scry
make
make install # as root

# if you want to build a debug version
make debug

# if you want to build an optimized version
make opt

# to build and run tests
make test && ./test_scry
```

### Usage

```bash
# echo in your input
$ echo "SuperSecretPassword" | scry

# enter interactively
$ scry
SuperSecretPassword [enter]

Flags:
-s      # provide your own salt (aka nonce)
-n      # cpu/memory cost (default 8192)
-r      # block size (default 8)
-p      # parallelism (default 2)
-h      # shows this help info
-v      # shows the version info
```

### Notes
This tool only reads input from stdin in order to avoid exposing sensitive information in the process list as arguments.

### License / Disclaimer
This project is licensed under the ISC license. (See LICENSE.md)
