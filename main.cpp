
/*  Main ('Runner') implementation file
 *  author: klosure
 *  license: ISC
 */

#include "main.hpp"

int main(int argc, char* argv[]) {
  const std::string VERSION = "0.1";
  Params p{ "", 8192, 8, 2, false, false, VERSION };
  int rv = 1;
  
  // parse the user input flags & options
  CLI::App app{ "Generate a key using the scrypt KDF" };
  app.ignore_case();

  app.add_option("-s, --salt", p.salt, "Specify your own salt (default behavior uses /dev/urandom)");
  app.add_option("-n, --cpucost", p.cost, "The CPU/Memory cost (default 8192)");
  app.add_option("-r, --blocksize", p.block, "Change the standard blocksize (default 8)");
  app.add_option("-p, --parallelism", p.parallel, "How much parallelism to use (default 2)");
  app.add_flag("-v,--version", "Display the version of scry");

  CLI11_PARSE(app, argc, argv);

  bool args_ok = false;
  try {
    args_ok = check_args(app, p);
  }
  catch(...){
    std::cout << "Problem parsing arguments! (Bad input values used?)";	
  }
  if (args_ok) {
    // good input, let's get to work
    auto scry_obj = std::make_unique<Scry>();
    std::string line;
    std::string str_pass;
    while (std::getline(std::cin, line) && !line.empty()) {
      str_pass = line;
    }
    bool pass_ok = (str_pass.length() < 513) && (str_pass.length() > 0);
    if (p.s_option_passed && pass_ok) {
      // call function using the salt provided by the user.
      rv = scry_obj->make_key(p.salt, str_pass, p.cost, p.block, p.parallel);
    }
    else if (!p.s_option_passed && p.nrp_option_passed) {
      // salt was not provided, but the -n, -r, or -p flag was used
      // so we have to generate one..
      size_t saltlen = 16;
      uint8_t created_salt[16];
      libscrypt_salt_gen(created_salt, saltlen);
      rv = scry_obj->make_key(created_salt, saltlen, str_pass, p.cost, p.block, p.parallel);
    }
    else if (pass_ok) {
      const char* password = line.c_str();
      rv = scry_obj->make_key(password, p.cost, p.block, p.parallel);
    }
    else {
      std::cout << "There was an issue reading your input. Was your password too long?\n";
    }
  }
  
  return rv;
}
