
/*  Scry header file
 *  author: klosure
 *  license: ISC
 */

#ifndef MY_SCRY
#define MY_SCRY

#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include "CLI11.hpp"
#include "libscrypt.h"
#include <cppcodec/base64_rfc4648.hpp>

class Scry {
public:
  int make_key(const char* password, uint64_t n, uint32_t r, uint32_t p) const;
  int make_key(std::string salt, std::string password, uint64_t n, uint32_t r, uint32_t p) const;
  int make_key(uint8_t* salt, size_t saltlen, std::string password, uint64_t n, uint32_t r, uint32_t p) const;
};

#endif // MY_SCRY
