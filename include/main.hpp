
/*  Main ('Runner') header file
 *  author: klosure
 *  license: ISC
 */

#ifndef MY_MAIN
#define MY_MAIN

#include "scry.hpp"

struct Params {
  std::string salt;
  uint64_t cost;
  uint32_t block;
  uint32_t parallel;
  bool s_option_passed;
  bool nrp_option_passed;
  const std::string VERSION;
};


/** --- Non-Member Helper Functions --- **/


inline void print_version(const std::string &version) {
  std::cout << "scry version: " << version << '\n';
}


bool check_for_ascii(std::string val) {
  bool rv = true;
  for (auto& c : val) {
    int range = 126 - char(c);
    if (range < 0 || range > 94) {
      std::cout << "Bad salt given. Contains non-7bit ascii characters.\n";
      rv = false;
      break;
    } 
  } 
  return rv;
}


bool check_args(CLI::App& app, Params& p) {
  if (app.count("-v")) {
    print_version(p.VERSION);
    return false;
  }
  if (app.count("-s")) {
    // perform our checks on the given salt
    if (!check_for_ascii(p.salt)) {
      return false;
    }
    p.s_option_passed = true;
  }
  if (app.count("-r")) {
    // must satisfy r * p < 2^30
    if (!(p.block * p.parallel < pow(2,30))) {
      std::cout << "r value was too big or small!\n";
      return false;
    }
    p.nrp_option_passed = true;
  }
  if (app.count("-p")) {
    // is a positive integer less than or equal
    // to ((2^32 - 1) * 32) / (128 * r)
    if (!(p.parallel <= ((pow(2,32) - 1) * 32 / (128 * p.block)))) {
      std::cout << "p value was too big or small!\n";
      return false;
    }
    p.nrp_option_passed = true;
  }
  if (app.count("-n")) {
    const uint64_t cost = p.cost;
    const auto nexp = ((128 * p.block) / 8);
    const auto max_n = pow(2,nexp);
    if (cost <= 1 || cost >= max_n) {
      std::cout << "n value was too big or small!\n";
      return false;
    }
    // is the n value a power of 2?
    if ((cost > 0) && (cost & (cost - 1))) {
      std::cout << "n value was not a power of 2 (e.g. 1024, 2048, 4096, etc).\n";
      return false;
    }
    p.nrp_option_passed = true;
  }
  return true;
}


#endif // MY_MAIN
